#!/usr/bin/env bash

if [ "$USER" != "root" ]
then
    echo "Necessário executar como root"
    exit 1
fi

# /usr/share/applications or ~/.local/share/applications

DIR_INSTALL='/usr/local/remoto-atua-linux'

rm -rf $DIR_INSTALL /usr/share/applications/remoto-atua.desktop