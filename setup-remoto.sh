#!/usr/bin/env bash

userid=$(id -u)
if [ $userid -ne 0 ]
then
    echo "Necessário executar como root"
    exit 1
fi

APT=`which apt-get`
YUM=`which yum`

if [ ! -z "$APT" ]
then
    echo "$APT"
    apt-get install x11vnc yad wget -y
elif [ ! -z "$YUM" ]
then
    echo "$YUM"
    yum install x11vnc yad wget -y
else
    echo "GERENCIADOR DE PACOTES DESCONHECIDO"
    exit 1
fi

# /usr/share/applications or ~/.local/share/applications

DIR_INSTALL='/usr/local/remoto-atua-linux'
URL_DOWNLOAD='https://adm.atua.com.br/remoto-atua-linux'

if [ -d $DIR_INSTALL ]
then
	rm -rf $DIR_INSTALL
fi


mkdir -p $DIR_INSTALL
cd $DIR_INSTALL

wget ${URL_DOWNLOAD}/remoto-atua.sh
wget ${URL_DOWNLOAD}/logo.bmp
wget ${URL_DOWNLOAD}/icon1.ico
wget ${URL_DOWNLOAD}/remoto-atua.desktop -O /usr/share/applications/remoto-atua.desktop
chmod -R 755 $DIR_INSTALL
#updatedb
update-desktop-database
