#!/usr/bin/env bash
DIR_INSTALL='/usr/local/remoto-atua-linux'
cd $DIR_INSTALL
X11VNC=`which x11vnc`
if [ -z "$X11VNC" ]
then
        echo "Necessário instalar x11vnc"
        sair=1
fi

YAD=`which yad`
if [ -z "$YAD" ]
then
        echo "Necessário instalar yad"
        sair=1
fi

ESCOLHIDO=$( \
    yad --list \
    --center \
    --image=logo.bmp \
    --title="Atua remoto" \
    --column="PORTA":NUM \
    --column="REMOTO":TEXT \
    --print-column=1 \
    --hide-column=1 \
    --width="500" \
    --button="Sair":1 \
    5501 "Remoto 01" \
    5502 "Remoto 02" \
    5503 "Remoto 03" \
    5504 "Remoto 04" \
    5505 "Remoto 05" \
    5506 "Remoto 06" \
    5507 "Remoto 07" \
    5508 "Remoto 08" \
    5509 "Remoto 09" \
    5510 "Remoto 10" \
    5511 "Remoto 11" \
    5512 "Remoto 12" \
    5513 "Remoto 13" \
    5514 "Remoto 14" \
    5515 "Remoto 15" \
    5516 "Remoto 16" \
    5517 "Remoto 17" \
    5518 "Remoto 18" \
    5519 "Remoto 19" \
    5520 "Remoto 20" \
    5521 "Remoto 21" \
    5522 "Remoto 22" \
    5523 "Remoto 23" \
    5524 "Remoto 24" \
    5525 "Remoto 25" \
    5526 "Remoto 26" \
    5527 "Remoto 27" \
    5528 "Remoto 28" \
    5529 "Remoto 29" \
    5530 "Remoto 30" \
    5531 "Remoto 31" \
    5532 "Remoto 32" \
    5533 "Remoto 33" \
    5534 "Remoto 34" \
    5535 "Remoto 35" \
    5536 "Remoto 36" \
    5537 "Remoto 37" \
    5538 "Remoto 38" \
    5539 "Remoto 39" \
    5540 "Remoto 40" \
    5541 "Remoto 41" \
    5542 "Remoto 42" \
    5543 "Remoto 43" \
    5544 "Remoto 44" \
    5545 "Remoto 45" \
    5546 "Remoto 46" \
    5547 "Remoto 47" \
    5548 "Remoto 48" \
    5549 "Remoto 49" \
    5550 "Remoto 50" \
    5551 "Remoto 51" \
    5552 "Remoto 52" \
    5553 "Remoto 53" \
    5554 "Remoto 54" \
)

echo $ESCOLHIDO
PORTA=`echo $ESCOLHIDO | tr -d '|'`
HOST='atua.com.br'
CMD="x11vnc -connect ${HOST}:${PORTA} -rfbport 0 -nopw -rfbversion 3.14"
#CMD="x11vnc -connect 10.0.0.46:5500 -rfbport 0 -nopw -rfbversion 3.14"
if [ ! -z $ESCOLHIDO ]
then
	$CMD &
	yad \
	    --pulsate \
	    --auto-close \
	    --auto-kill \
	    --title="Atua Remoto" \
	    --width="500" \
	    --center \
	    --image=logo.bmp \
	    --button="Cancel:1" \
	    --text="Sua tela está sendo visualizada."

	if [[ $? -eq 1 ]];then
		pidof x11vnc | xargs kill -2
		exit
	fi
fi


